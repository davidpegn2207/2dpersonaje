using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Personaje : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Animator anim;
    public GameObject popupWindowObject;
    // Start is called before the first frame update
    void Start()
    {
        popupWindowObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void play()
    {
        StartCoroutine("Nose");
       
    }
    public  void siguiente()
    {
        StartCoroutine("Saludo");
    }
    IEnumerator Nose()
    {
        anim.SetBool("Nose", true);
        yield return new WaitForSeconds(3);
        anim.SetBool("Nose", false);
    }
    IEnumerator Saludo()
    {
        anim.SetBool("Saludo", true);
        yield return new WaitForSeconds(3);
        anim.SetBool("Saludo", false);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {

        popupWindowObject.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {

        popupWindowObject.SetActive(false);
    }

}
